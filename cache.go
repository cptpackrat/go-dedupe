// Package dedupe implements a TTL cache designed for de-duplicating the creation of data that
// has a long-running (and presumably fallible) creation process. The goal of this cache is
// not performance, but rather a strict guarantee that work is only done once for a given key
// within a given lifetime.
package dedupe

import (
	"sync"
	"sync/atomic"
	"time"
)

type Cache[K comparable, V any] struct {
	mutex        sync.Mutex
	cache        map[K]*cached[V]
	stop         chan bool
	onEvict      func(key K, val V)
	onEvictCycle func(total, delta int)
}

type cached[V any] struct {
	val   V
	set   bool
	exp   time.Time
	mutex sync.Mutex
	queue atomic.Int32
}

// Returns a new Cache instance with the given GC interval.
func NewCache[K comparable, V any](every time.Duration) *Cache[K, V] {
	c := Cache[K, V]{
		cache: map[K]*cached[V]{},
	}
	c.start(every)
	return &c
}

// Returns a new Cache instance with the given GC interval and two optional
// callbacks, the former of which is called every time a key is evicted, and
// the latter at the completion of every GC cycle.
//
// Note that these callbacks will not be called from the same goroutine that
// created the Cache instance.
func NewCacheWithCallback[K comparable, V any](every time.Duration, onEvict func(key K, val V), onEvictCycle func(total, delta int)) *Cache[K, V] {
	c := Cache[K, V]{
		cache:        map[K]*cached[V]{},
		onEvict:      onEvict,
		onEvictCycle: onEvictCycle,
	}
	c.start(every)
	return &c
}

// Check if the given key exists within the cache. If yes, the cached result
// will be returned, otherwise the provided callback will be executed. If the
// callback returns a nil error then its return value will be cached with the
// given TTL; in either case the return values of the callback will be the
// return values of this function.
//
// If this function is called concurrently for the same key from multiple threads
// their callbacks will be executed in order until the first one is found that does
// not return an error. The return value from this callback will be cached, and
// returned as the result of all pending Check calls for that key.
func (c *Cache[K, V]) Check(key K, ttl time.Duration, fn func() (V, error)) (V, error) {
	var p *cached[V]
	c.mutex.Lock()
	if p = c.cache[key]; p != nil {
		c.mutex.Unlock()
		p.queue.Add(1)
		p.mutex.Lock()
	} else {
		p = &cached[V]{}
		p.queue.Store(1)
		p.mutex.Lock()
		c.cache[key] = p
		c.mutex.Unlock()
	}
	defer p.mutex.Unlock()
	if p.set {
		p.queue.Add(-1)
		return p.val, nil
	}
	v, err := fn()
	if err == nil {
		p.val = v
		p.set = true
		p.exp = time.Now().Add(ttl)
	}
	p.queue.Add(-1)
	return v, err
}

// Stops the goroutine processing evictions for this cache.
func (c *Cache[K, V]) Stop() {
	close(c.stop)
}

func (c *Cache[K, V]) start(every time.Duration) {
	stop := make(chan bool)
	tick := time.NewTicker(every)
	go func() {
		for {
			select {
			case now := <-tick.C:
				c.gc(now)
			case <-stop:
				tick.Stop()
				return
			}
		}
	}()
	c.stop = stop
}

func (c *Cache[K, V]) gc(now time.Time) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	total := 0
	delta := 0
	for key, p := range c.cache {
		total++
		if p.queue.Load() > 0 {
			continue
		}
		if !p.set || p.exp.Before(now) {
			delta++
			delete(c.cache, key)
			if p.set && c.onEvict != nil {
				c.onEvict(key, p.val)
			}
		}
	}
	if c.onEvictCycle != nil {
		c.onEvictCycle(total, delta)
	}
}
