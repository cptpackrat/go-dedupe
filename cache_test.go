package dedupe

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCacheConcurrent(t *testing.T) {
	cache := NewCache[string, int](time.Minute)
	count := 0
	value := 0
	wg := sync.WaitGroup{}
	wg.Add(10)
	for i := 1; i < 11; i++ {
		go func(i int) {
			v, err := cache.Check("test", time.Second, func() (int, error) {
				time.Sleep(time.Millisecond * 100)
				assert.Equal(t, 0, count, "while i == %d", i)
				assert.Equal(t, 0, value, "while i == %d", i)
				count++
				value = i
				return i, nil
			})
			assert.NoError(t, err)
			assert.NotEqual(t, 0, value, "after i == %d", i)
			assert.Equal(t, value, v, "after i == %d", i)
			wg.Done()
		}(i)
	}
	wg.Wait()
	assert.Equal(t, 1, count)
	assert.NotEqual(t, 0, value)
	v, err := cache.Check("test", time.Second, func() (int, error) {
		count++
		return 99, nil
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, count)
	assert.Equal(t, value, v)
}

func TestCacheConcurrentErrors(t *testing.T) {
	cache := NewCache[string, int](time.Minute)
	count := 0
	wg := sync.WaitGroup{}
	wg.Add(10)
	for i := 1; i < 11; i++ {
		go func(i int) {
			v, err := cache.Check("test", time.Second, func() (int, error) {
				time.Sleep(time.Millisecond * 100)
				count++
				if i < 5 {
					return 0, fmt.Errorf("not 5 yet")
				}
				return i, nil
			})
			if i < 5 {
				assert.ErrorContains(t, err, "not 5 yet")
			} else {
				assert.NoError(t, err)
				assert.Equal(t, 5, v, "after i == %d", i)
			}
			wg.Done()
		}(i)
		time.Sleep(time.Millisecond * 50)
	}
	wg.Wait()
	assert.Equal(t, 5, count)
	v, err := cache.Check("test", time.Second, func() (int, error) {
		count++
		return 99, nil
	})
	assert.NoError(t, err)
	assert.Equal(t, 5, count)
	assert.Equal(t, 5, v)
}

func TestCacheExpiry(t *testing.T) {
	evict_t := 0
	evict_d := 0
	evict_k := 0
	cache := NewCacheWithCallback(time.Millisecond*50, func(k, v int) {
		evict_k++
	}, func(t, d int) {
		evict_t = t
		evict_d += d
	})
	count := 0
	for i := 1; i < 11; i++ {
		v, err := cache.Check(i, time.Millisecond*time.Duration(i*100+25), func() (int, error) {
			count++
			return 1, nil
		})
		assert.NoError(t, err)
		assert.Equal(t, 1, v, "after i == %d", i)
	}
	assert.Equal(t, 10, count)
	time.Sleep(time.Millisecond * 75)
	for i := 1; i < 11; i++ {
		v, err := cache.Check(i, 0, func() (int, error) {
			count++
			return 2, nil
		})
		assert.NoError(t, err)
		assert.Equal(t, 1, v, "after i == %d", i)
	}
	assert.Equal(t, 10, count)
	time.Sleep(time.Millisecond * 500)
	for i := 1; i < 11; i++ {
		v, err := cache.Check(i, 0, func() (int, error) {
			count++
			return 3, nil
		})
		assert.NoError(t, err)
		if i < 6 {
			assert.Equal(t, 3, v, "after i == %d", i)
		} else {
			assert.Equal(t, 1, v, "after i == %d", i)
		}
	}
	assert.Equal(t, 15, count)
	time.Sleep(time.Millisecond * 500)
	for i := 1; i < 11; i++ {
		v, err := cache.Check(i, 0, func() (int, error) {
			count++
			return 4, nil
		})
		assert.NoError(t, err)
		assert.Equal(t, 4, v, "after i == %d", i)
	}
	assert.Equal(t, 25, count)
	time.Sleep(time.Millisecond * 500)
	assert.Equal(t, evict_t, 0)
	assert.Equal(t, evict_d, 25)
	assert.Equal(t, evict_k, 25)
}

func TestCacheExpiryHammer(t *testing.T) {
	evict_t := 0
	evict_d := 0
	evict_k := 0
	cache := NewCacheWithCallback(time.Millisecond*1000, func(k, v int) {
		evict_k++
	}, func(t, d int) {
		evict_t = t
		evict_d += d
	})
	count := atomic.Int32{}
	for i := 0; i < 100; i++ {
		go func(i int) {
			for k := 0; k < 1000; k++ {
				v, err := cache.Check(i*1000+k, time.Millisecond*750, func() (int, error) {
					count.Add(1)
					return 1, nil
				})
				assert.NoError(t, err)
				assert.Equal(t, 1, v, "after i == %d, k == %d", i, k)
				v, err = cache.Check(i*1000+k, time.Millisecond*750, func() (int, error) {
					count.Add(1)
					return 2, nil
				})
				assert.NoError(t, err)
				assert.Equal(t, 1, v, "after i == %d, k == %d", i, k)
			}
		}(i)
	}
	time.Sleep(time.Millisecond * 500)
	assert.Equal(t, int32(100000), count.Load())
	assert.Equal(t, 0, evict_t)
	assert.Equal(t, 0, evict_d)
	assert.Equal(t, 0, evict_k)
	time.Sleep(time.Millisecond * 1000)
	assert.Equal(t, int32(100000), count.Load())
	assert.Equal(t, 100000, evict_t)
	assert.Equal(t, 100000, evict_d)
	assert.Equal(t, 100000, evict_k)
}

func TestCacheStop(t *testing.T) {
	evict_d := 0
	evict_k := 0
	cache := NewCacheWithCallback(time.Millisecond*50, func(k, v int) {
		evict_k++
	}, func(t, d int) {
		evict_d += d
	})
	count := 0
	for i := 1; i < 11; i++ {
		v, err := cache.Check(i, time.Millisecond*time.Duration(i*100+25), func() (int, error) {
			count++
			return 1, nil
		})
		assert.NoError(t, err)
		assert.Equal(t, 1, v, "after i == %d", i)
	}
	assert.Equal(t, 10, count)
	time.Sleep(time.Millisecond * 75)
	assert.Equal(t, evict_d, 0)
	assert.Equal(t, evict_k, 0)
	time.Sleep(time.Millisecond * 500)
	assert.Equal(t, evict_d, 5)
	assert.Equal(t, evict_k, 5)
	cache.Stop()
	time.Sleep(time.Millisecond * 500)
	assert.Equal(t, evict_d, 5)
	assert.Equal(t, evict_k, 5)
	time.Sleep(time.Millisecond * 500)
	assert.Equal(t, evict_d, 5)
	assert.Equal(t, evict_k, 5)
	assert.Panics(t, func() {
		cache.Stop()
	})
}
